if pgrep -x "node" > /dev/null
then
    echo "Forever still running..."
else
    echo "Detected forever stopped... Restarting..."
    sudo runuser -l ec2-user -c "forever stopall"
    sudo runuser -l ec2-user -c "cd /home/ec2-user/osrsbotting_bot_tools/tools/ && forever start src/index.js"
fi