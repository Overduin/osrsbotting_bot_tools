import fs from 'fs';

const statusses = [
	'WAITING_FOR_INITIAL_MONEY',
	'USING_BOND',
	'WORLD_HOPPING',
	'LOGGING_IN_TO_MEMBER_WORLD',
	'BUYING_HIDES',
	'BUYING_STAMINA_POTIONS',
	'MULING_LEFTOVER_MONEY',
	'BANKING_INVENTORY',
	'SELLING_HIDES',
	'TANNING',
]
