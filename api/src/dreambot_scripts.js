import itemIDMap from '../json/itemIdMap.json' ;
import fetch from 'node-fetch';

const statusMatches = [
	{
		match: 'GET TASK',
		status: 'GETTING_SLAYER_TASK',
	},
	{
		match: 'HEAL',
		status: 'HEALING',
	},
	{
		match: 'BANK UTIL',
		status: 'BANKING',
	},
	{
		match: 'GE UTIL',
		status: 'USING GE',
	},
	{
		match: 'EQUIP GEAR',
		status: 'GEARING',
	},
	{
		match: 'TRAVEL',
		status: 'TRAVELLING'
	},
	{
		match: 'FIGHT',
		status: 'FIGHTING',
	},
	{
		match: 'ATTACK',
		status: 'FIGHTING',
	},
]

export const handleDreambotScripts = (app, redisClient, io) => {

	const processLog = async (event) => {

		const { messages, username, socketId } = event;

		const quantityMatchText = 'quantity updated to ';

		for (const { message } of messages) {

			if (message.includes('Restoring all stats')) {

				const muleStatus = await redisClient.hGet('mule_status', username);

				if (muleStatus === 'MULING') {

					io.to(socketId).emit('run_command', { command: "/home/ec2-user/DreamBot/BotData/client.jar -fps 5 -noClickWalk -script Bun%Automation%Tool -username $DB_USERNAME -password $DB_PASSWORD -covert -accountUsername $USERNAME -accountPassword $PASSWORD -render none -no-fresh -params config=/home/ec2-user/DreamBot/Scripts/Bun/AutomationTool/mule" });

					await redisClient.hSet('mule_status', username, `ACTIVE`);
				}
			}

			if (message.includes('Mule accepted trade')) {

				const muleStatus = await redisClient.hGet('mule_status', username);

				if (muleStatus === 'ACTIVE') {

					const lastCommand = await redisClient.hGet('account_commands_running', username);

					io.to(socketId).emit('run_command', { command: lastCommand });

					await redisClient.hSet('mule_status', username, `IDLE`);
				}
			}

			if (message.includes('Java heap space')) {

				const lastCommand = await redisClient.hGet('account_commands_running', username);

				io.to(socketId).emit('run_command', { command: lastCommand });
			}

			if (message.includes('Looting')) {

				// 2022-10-30 21:43:07 [INFO] ABSTRACT LOOT: Looting 41 x Mud rune(3690gp) ...

				const [date, time, logLevel, lootText, ...rest] = message.split(' ');




			}

			if (message.includes('LOOTED')) {

				// "2022-09-26 21:03:17 [INFO] LOOTED: 12 White berries"

				const [date, time, logLevel, lootText, ...rest] = message.split(' ');

				const lootMessage = rest.join(' ');

				const [quantity, ...itemText] = lootMessage.split(' ')

				const item = itemText.join(' ');

				// console.log(`Looted ${quantity} x ${item}...`);

				await redisClient.hSet('looted_items', `${username}:${new Date().getTime()}`, `${item}:${quantity}`);

				io.to('hosts').emit('looted_items_update', {
					username,
					item,
					quantity,
				})
			}

			if (message.includes('DEATH')) {

				await redisClient.hSet('account_status', username, `DEATH_RECOVERY:${new Date().getTime()}`);
			}

			if (message.includes('GE CONFIG') && message.includes('Total coins')) {

				const [date, time, logLevel, lootText, ...rest] = message.split(' ');
				// GE CONFIG: Total coins = 914451
				const [stuff, coins] = rest.join('').split('=');

				await redisClient.hSet('banked_coins', username, Number(coins));

				io.to('hosts').emit('banked_coins_update', {
					username,
					quantity: Number(coins)
				})
			}

			const currentTime = new Date().getTime();

			if (message.includes('INVALID_LOGIN')) {

				await redisClient.hSet('account_status', username, `INVALID_LOGIN:${currentTime}`);
			}

			if (message.includes('MEMBERS_WORLD')) {

				await redisClient.hSet('account_status', username, `MEMBERS_WORLD:${currentTime}`);

				io.to(socketId).emit('stop_all_commands');
			}

			if (message.includes('ACCOUNT_LOCKED')) {

				await redisClient.hSet('account_status', username, `ACCOUNT_LOCKED:${currentTime}`);

				io.to(socketId).emit('stop_all_commands');
			}

			/** Stupid error */
			if (message.includes('no WOODCUTTING_GUILD available')) {

				const lastCommand = await redisClient.hGet('account_commands_running', username);

				io.to(socket.id).emit('run_command', { command: lastCommand });

				await redisClient.hSet('account_status', username, `RESTARTING_SCRIPT:${new Date().getTime()}`);
			}

			if (message.includes('DISABLED')) {

				await redisClient.hSet('account_status', username, `DISABLED:${currentTime}`);

				io.to(socketId).emit('stop_all_commands');
			}

			for (const statusMatch of statusMatches) {

				if (message.includes(statusMatch.match)) {

					await redisClient.hSet('account_status', username, `${statusMatch.status}:${currentTime}`);
				}
			}
		}
	}

	app.get('/rates-data', async (req, res) => {

		const latestPriceResponse = await fetch('https://prices.runescape.wiki/api/v1/osrs/latest');

		const { data } = await latestPriceResponse.json();

		redisClient.hGetAll('looted_items').then(lootedItems => {

			const dateStringList = new Set();

			if (!data) {

				res.json({
					userMap: {},
					dates: [],
				});

				return;
			}

			let userMap = {};

			for (const rawLoot in lootedItems) {

				const [username, timestamp] = rawLoot.split(':')

				const [item, quantity] = lootedItems[rawLoot].split(':');

				const date = new Date(Number(timestamp));

				date.setHours(date.getHours(), 0, 0, 0);

				const dateString = date.getTime();

				dateStringList.add(dateString);

				const itemId = itemIDMap[item];

				const priceData = data[itemId] || { high: 1, low: 1 };

				const averagePrice = ((priceData.high + priceData.low) / 2) >> 0;

				if (!userMap[username]) {

					userMap[username] = {};
				}

				userMap[username][dateString] = (userMap?.[username]?.[dateString] || 0) + (averagePrice * Number(quantity));
			}

			for (const date of dateStringList) {

				for (const username in userMap) {

					if (!userMap[username][date]) {

						userMap[username][date] = 0;
					}
				}
			}

			res.json({
				userMap,
				dates: Array.from(dateStringList),
			})
		});
	})


	app.post('/auth', async (req, res) => {

		const authUsers = await redisClient.hGetAll('auth_users');

		const { user, password } = req.body;

		if (Boolean(authUsers)) {

			for (const authUser in authUsers) {

				const authPassword = authUsers[authUser];

				if (user === authUser && authPassword === password) {

					res.json({
						token: process.env.API_KEY,
					})

					return;
				}
			}
		}

		res.json({
			error: 'not found',
		})
	})


	app.get('/loot-data', async (req, res) => {

		const latestPriceResponse = await fetch('https://prices.runescape.wiki/api/v1/osrs/latest');

		const lootedItems = await redisClient.hGetAll('looted_items');

		const prices = await latestPriceResponse.json();

		const itemPrices = prices.data;

		const lootMap = {}

		const userLootMap = {}

		let totalValue = 0;

		const itemMap = new Set();

		for (const record in lootedItems) {

			const [username, timestamp] = record.split(':');

			const [item, q] = lootedItems[record].split(':');

			const quantity = Number(q);

			const itemId = itemIDMap[item];

			itemMap.add(`${item}`);

			if (!lootMap[item]) {

				lootMap[item] = {
					quantity: 0,
					value: 0,
				}
			}

			lootMap[item].quantity += quantity;

			if (!userLootMap[username]) {

				userLootMap[username] = {};
			}

			if (!userLootMap[username][item]) {

				userLootMap[username][item] = {
					quantity: 0,
					value: 0,
				};
			}

			userLootMap[username][item].quantity += quantity;

			if (Boolean(itemPrices[itemId])) {

				const priceData = itemPrices[itemId];

				const averagePrice = ((priceData.high + priceData.low) / 2) >> 0;

				userLootMap[username][item].value = averagePrice >> 0;

				lootMap[item].value = averagePrice >> 0;

				totalValue += (quantity * averagePrice) >> 0;

			} else {

				totalValue += quantity;

				userLootMap[username][item].value = 1;

				lootMap[item].value = 1;
			}
		}

		res.json({
			lootMap,
			userLootMap,
			totalValue,
		})
	})


	app.get('/coins', async (req, res) => {

		const bankedCoins = await redisClient.hGetAll('banked_coins');

		res.send({
			bankedCoins,
		})
	})

	app.get('/clients', async (req, res) => {

		const accountStatuses = await redisClient.hGetAll('account_status');

		const accountSocketMap = await redisClient.hGetAll('connected_clients');

		res.send({
			sockets: Object.keys(accountSocketMap).map(socketId => ({
				username: accountSocketMap[socketId],
				socketId,
				status: accountStatuses[accountSocketMap[socketId]],
			})),
			accountStatuses,
		});
	})

	app.get('/', (req, res) => {

		console.log('Route: /');

		res.sendStatus(200)
	})


	app.post('/stop_all', async (req, res) => {

		if (req.headers['authorization'] !== process.env.API_KEY) {

			res.sendStatus(401);

			return;
		}

		const { socketId, username } = req.body;

		io.to(socketId).emit('stop_all_commands');

		await redisClient.hSet('account_status', username, `STOPPED:${new Date().getTime()}`)

		res.json({
			socketId,
		})
	})


	app.post('/update_git', async (req, res) => {

		if (req.headers['authorization'] !== process.env.API_KEY) {

			res.sendStatus(401);

			return;
		}

		const { socketId } = req.body;

		io.to(socketId).emit('update_git');

		res.json({
			socketId,
		})
	})

	app.post('/update_bot', async (req, res) => {

		console.log({
			reqH: req.headers,
		})

		if (req.headers['authorization'] !== process.env.API_KEY) {

			res.sendStatus(401);

			return;
		}

		const { socketId, username } = req.body;

		io.to(socketId).emit('stop_all_commands');

		await redisClient.hSet('account_status', username, `STOPPED:${new Date().getTime()}`)

		io.to(socketId).emit('update_bot');

		res.json({
			socketId,
		})
	})

	app.post('/run_command', async (req, res) => {

		if (req.headers['authorization'] !== process.env.API_KEY) {

			res.sendStatus(401);

			return;
		}

		const { socketId, command, username, type } = req.body;

		io.to(socketId).emit('run_command', { command });

		if (type === 'MULE') {

			await redisClient.hSet('mule_status', username, `MULING`);

		} else {

			await redisClient.hSet('mule_status', username, `IDLE`);

			await redisClient.hSet('account_commands_running', username, command);
		}

		await redisClient.hSet('account_status', username, `STARTING_SCRIPT:${new Date().getTime()}`);

		res.json({
			socketId,
			command
		})
	})



	io.on('connection', (socket) => {

		console.log('client connected', socket.id);


		socket.on('stopped', (event) => {

			const { username } = event;

			redisClient.hSet('mule_status', username, 'IDLE');
		})

		socket.on('subscribe_client', async (event) => {

			socket.join('clients');

			const username = (event || {}).username;

			if (username) {

				const socketMap = await redisClient.hGetAll('connected_clients');

				for (const socketId in socketMap) {

					if (socketMap[socketId] === username) {

						await redisClient.hDel('connected_clients', socketId);

					}

				}
			}

			redisClient.hSet('connected_clients', socket.id, (event || {}).username);

			io.to('hosts').emit('client_join', { socketId: socket.id, username: (event || {}).username })

			console.log('subscribe_client')
		})

		socket.on('subscribe_host', () => {

			if (socket.handshake.auth.token === process.env.API_KEY) {

				socket.join('hosts');
			}

			console.log('subscribe_host')
		})

		socket.on('disconnect', () => {

			socket.leave('clients');

			socket.leave('hosts');

			redisClient.hDel('connected_clients', socket.id);

			io.to('hosts').emit('client_disconnected', { socketId: socket.id })
		})

		socket.on('client_stats', async (event) => {

			const username = event.username;

			const accountStatus = await redisClient.hGet('account_status', username);

			event.status = accountStatus;

			io.to('hosts').emit('client_stats', event);
		})

		socket.on('script_issues_detected', async (event) => {

			const username = event.username;

			const accountStatus = await redisClient.hGet('account_status', username);

			if (!accountStatus.includes('STOPPED') && !accountStatus.includes('DISABLED')) {

				const muleStatus = await redisClient.hGet('mule_status', username);

				console.log({
					muleStatus,
					username
				})

				if (!['ACTIVE', 'MULING'].includes(muleStatus)) {

					await redisClient.hSet('account_status', username, `RESTARTING_SCRIPT:${new Date().getTime()}`);

					const lastCommand = await redisClient.hGet('account_commands_running', username);

					io.to(socket.id).emit('run_command', { command: lastCommand });
				}
			}
		})

		socket.on('log_update', async (event) => {

			processLog(event)

			io.to('hosts').emit('host_update', event)
		})
	})



}