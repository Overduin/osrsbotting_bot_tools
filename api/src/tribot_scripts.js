import multer from 'multer';
import { exec, execSync } from 'child_process';
import mysql from 'mysql';
import * as dotenv from 'dotenv';


dotenv.config();

const dbConnection = mysql.createConnection({
	host: process.env.DB_HOST,
	user: process.env.DB_USERNAME,
	password: process.env.DB_PASSWORD,
	database: process.env.DB_NAME
});

dbConnection.connect();

const createUser = (username, callback) => {

	dbConnection.query('INSERT INTO `accounts` (`login`) VALUES ?', [[[username]]], (err, res) => {

		callback(res.id)
	})
}

const createTbData = (userId, data) => {

	if(data.teleblockData.length) {

		const [ tbText ] = data.teleblockData;

		if (tbText.includes('fi Tele Block spell has been cast on you by')) {

			const [pker, durationText] = tbText.split('fi Tele Block spell has been cast on you by ')
				.filter(Boolean)
				.join(' ')
				.split('. ')

			let duration = 300;

			if (`${durationText}`.includes('2') || `${durationText}`.includes('30')) {

				duration = 150;
			}

			console.log({
				status: 'TELEBLOCK',
				duration,
				pker,
			})

			dbConnection.query('SELECT * FROM `teleblocks` WHERE `accountId` = ? AND `pkerName` = ? AND `duration` = ? ORDER BY `id` DESC LIMIT 1', [userId, pker, duration], (err, res) => {

				if (!res.length) {

					dbConnection.query('INSERT INTO `teleblocks` (`accountId`, `duration`, `pkerName`) VALUES ?', [[[userId, duration, pker]]])
				}
			})
		}
	}
}

const createStatus = (userId, data) => {

	if (data.scriptData.length) {

		const [scriptVersion, runTime, statusText] = data.scriptData;

		const status = statusText.split(':');

		if (Boolean(status[1])) {

			const newStatus = status[1].trim();

			dbConnection.query('SELECT `name` FROM `accountStatus` WHERE `accountId` = ? ORDER BY `id` DESC LIMIT 1', [userId], (err, res) => {

				if(!res.length) {

					dbConnection.query('INSERT INTO `accountStatus` (`accountId`, `name`) VALUES ?', [[[ userId, newStatus ]]]);

				} else {

					const currentStatus = res[0].name;

					if(currentStatus !== newStatus) {

						dbConnection.query('INSERT INTO `accountStatus` (`accountId`, `name`) VALUES ?', [[[userId, newStatus]]]);
					}
				}

			})
		}
	}
}

const createDrop = (userId, data) => {

	if(data.lootData.length) {

		const { lootData } = data;

		const lootMap = lootData.filter(item => {

			return (
				item !== 'Valuable drop' &&
				!item.includes('frozen') &&
				!item.includes('freeze') &&
				!item.includes('superior') &&
				!item.includes('been awoken') &&
				item !== 'i' &&
				item.length > 4
			);

		}).map(item => {

			if(item.includes('x')) {

				const [qtyText, ...rest] = item.split('x');

				return [qtyText, rest.join('x')];
			}

			return [
				`1`,
				item
			]

		}).filter(e => {

			return e.length > 1 && e[1].length > 4;

		}).map(([quantityText, itemText ]) => {

			return {
				quantity: Number(
					quantityText
					.replace('Valuable drop:', '')
					.replace('ble', 44)
					.replace('Wl', 44)
					.replace('h', 44)
					.replace('b', 4)
					.replace('k', 4)
					.replace('l', 4)
					.trim()
				),
				name: (
					itemText
					.replace('Valuable drop:', '')
					.replace('.', '')
					.split('(')[0]
					.split('Gk')[0]
					.trim()
				),
			}
		})

		const nonNumberLootMapItems = lootMap.filter((e) => {

			return !Boolean(e.quantity);
		})

		const onScreenItemLoot = lootMap.filter((e) => {

			return Boolean(e.quantity);
		})

		const onScreenItemLootCount = onScreenItemLoot.length;

		if (Boolean(nonNumberLootMapItems.length)) {

			dbConnection.query('INSERT INTO `unknown_drops` (`text`, `accountId`) VALUES ?', [ nonNumberLootMapItems.map(e => [`${e.quantity}:${e.name}`, userId ]) ] , (err, res) => {

				console.log({
					err, res
				})
			})

		}

		if (Boolean(onScreenItemLootCount)) {

			dbConnection.query('SELECT * from `drops` WHERE `accountId` = ? ORDER BY `id` DESC LIMIT ?', [userId, onScreenItemLootCount], (err, res) => {

				if (!res.length) {

					dbConnection.query('INSERT INTO `drops` (`name`, `quantity`, `accountId`) VALUES ? ', [Object.values(onScreenItemLoot).map(data => ([data.name, data.quantity, userId]))])

				} else {

					const dbLootMap = res;

					const difference = onScreenItemLoot.filter(item => {
						return !dbLootMap.some(dbItem => {
							return (
								item.name === dbItem.name && item.quantity === dbItem.quantity
							);
						});
					});

					if (Boolean(difference.length)) {

						dbConnection.query('INSERT INTO `drops` (`name`, `quantity`, `accountId`) VALUES ? ', [Object.values(difference).map(data => ([data.name, data.quantity, userId]))])
					}
				}
			})
		}

		// console.log({
		// 	lootMap
		// })


	}
}

const saveAccountData = (data) => {

	const username = data.username;

	if(Boolean(data.scriptData.length)) {

		console.log(`${username} running for: `, data.scriptData[1]);

	} else {

		console.log(`${username} not running...`);
	}


	// console.log('saving data for', username);

	const userQuery = dbConnection.query('SELECT id FROM `accounts` WHERE `login` = ?', [ username ], (err, res) => {

		if(!res.length) {

			createUser(username, (userId) => {

				createDrop(userId, data);

				createTbData(userId, data);

				createStatus(userId, data);
			});

		} else {

			const userId = res[0].id;

			createDrop(userId, data);

			createTbData(userId, data);

			createStatus(userId, data);

		}

	})

}


const storage = multer.diskStorage({
	destination: (req, file, callback) => {

		const path = `${process.cwd()}/screenshots`

		// console.log({ path })

		callback(null, path);
	},
	filename: (req, file, callback) => {

		callback(null, file.originalname);
	}
})

const extractTextFromScreenData = (filepath) => {

	try {

		const screenText = execSync('python3 ./python/script.py');

		return screenText.toString();

	} catch (error) {

		console.log(error);

		return null;
	}
}

const imageUpload = multer({ storage })

export const handleTribotScripts = (app, redisClient, io) => {

	app.post('/script-event', async (req, res) => {

		const { socketId, eventName, eventValues } = req.body;

		io.to(socketId).emit(eventName, eventValues);

		res.json({
			socketId, eventName, eventValues
		})
	})

	app.post('/screen-data', imageUpload.single('file'), async (req, res) => {

		const textData = extractTextFromScreenData(req.file.destination);

		// console.log({
		// 	reqF: req.file
		// })

		// execSync(`mv ${req.file.path} ./status/${req.body.username}_latest.png`);

		if(Boolean(textData)) {

			saveAccountData({
				username: req.body.username,
				...JSON.parse(textData)
			})

		}

		exec(`rm ${req.file.path}`);

		res.send('File uploaded successfully');
	});
}
