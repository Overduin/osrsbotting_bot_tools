import { spawn,exec, spawnSync, execSync } from 'child_process';
import * as dotenv from 'dotenv';
import { getDBUserInfo, getUserInfo } from '../scripts/userHelper.js';
import fs from 'fs';
import { v4 as uuidv4 } from 'uuid';
import os from 'os';


dotenv.config();

const replaceWithCredentials = command => {

	const { dbUsername, dbPassword } = getDBUserInfo();

	const { username, password } = getUserInfo();

	command = command.replace('$USERNAME', username)
	command = command.replace('$PASSWORD', password)
	command = command.replace('$DB_USERNAME', dbUsername)
	command = command.replace('$DB_PASSWORD', dbPassword)

	return command;
}

export const handleBotUpdates = async () => {

	console.log('Updating DreamBot...');

	return new Promise( async (resolve, reject) => {

		const requestTimeout = setTimeout(() => {

			clearTimeout(requestTimeout);

			reject('timeout');

		}, 30e3)

		spawnSync('xvfb-run', ['java', '-jar', '/home/ec2-user/DBLauncher.jar'], {
			timeout: 10e3,
			detached: true,
		});

		console.log('Updated dreambot');

		await stopScripts('java');

		await stopScripts('xvfb');

		resolve();
	})
}

export const reboot = () => {

	execSync('sudo reboot');
}

export const handleGitUpdates = async () => {

	return new Promise((resolve, reject) => {

		exec(`cd ~/osrsbotting_bot_tools/tools && git pull`, (e, stdo, stder) => {

			execSync('cp -rf /home/ec2-user/osrsbotting_bot_tools/tools/automations/druid_killer/config/mule.cfg /home/ec2-user/DreamBot/Scripts/Bun/AutomationTool');
			execSync('cp -rf ~/osrsbotting_bot_tools/tools/automations/druid_killer/config/profile.json ~/DreamBot/SubUndead/profile.txt');
			execSync('cp -rf ~/osrsbotting_bot_tools/tools/automations/druid_killer/config/default.json ~/DreamBot/SubUndead/default.txt');
			execSync('forever restartall');

			resolve();
		});
	})
}

export const startBotCommand = async (command, socket) => {

	const { username } = getUserInfo();

	try {

		await stopScripts("java");

		await stopScripts("xvfb");

		command = replaceWithCredentials(command)

		console.log({
			command
		})

		spawn('xvfb-run', ['java', '-Xms256M','-Xmx384M', '-jar', ...command.split(' ').map(e => e.replace(/%/g, ' '))], {
			stdio: 'ignore',
		});

	} catch (error) {

		console.log(error)
	}
}

export const stopScripts = (match = '') => {

	return new Promise((resolve, reject) => {

		exec(`ps -A | grep "${match}"`, (error, stdout, stderr) => {

			stdout.split('\n').forEach(line => {

				console.log({ line })

				const [ pid ] = line.split(' ').filter(Boolean);

				if (Boolean(pid)) {

					console.log('killing pid:', pid)

					exec(`sudo kill -9 ${pid}`);
				}
			})

			resolve();
		})
	})
}