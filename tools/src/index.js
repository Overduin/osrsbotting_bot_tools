import * as dotenv from 'dotenv';
import { io } from 'socket.io-client';
import { startBotCommand, handleGitUpdates, handleBotUpdates, stopScripts, reboot } from './scripts/scriptHandler.js';
import os from 'os';
import { getAccountName, getUserInfo } from './scripts/userHelper.js';
import chokidar from 'chokidar';
import { exec, execSync } from 'child_process';
import { v4 as uuidv4 } from 'uuid';
import osu from 'node-os-utils';

dotenv.config();

const socket = io(process.env.API_URL, {
	reconnection: true,
})

let latestCommand = '';

socket.on('connect', () => {

	console.log('socketId', socket.id)

	console.log('connected')

	const { username } = getUserInfo();

	socket.emit('subscribe_client', { username });
})

socket.on('error', e => {

	console.log('error', e)
})


socket.on('update_git', async () => {

	await handleGitUpdates();
})

socket.on('update_bot', async () => {

	await handleBotUpdates();
})

socket.on('stop_all_commands', async () => {

	try {

		await stopScripts("java");

		await stopScripts("xvfb");

		socket.emit('stopped', { username });

	} catch (error) {

		console.log('Unable to stop all scripts', error)
	}
})

socket.on('run_command', event => {

	const { command } = event;

	try {

		startBotCommand(command);

	} catch (error) {

		console.log('Cant run command.', error)
	}
})

const emitClientStats = async (sock) => {

	const { username } = getUserInfo();

	const account = getAccountName();

	const cpu = osu.cpu;

	const freecpu = await cpu.free(250);

	sock.emit('client_stats', {
		username,
		socketId: sock.id,
		uptime: os.uptime(),
		platform: os.platform(),
		release: os.release(),
		version: os.version(),
		totalmem: os.totalmem(),
		freemem: os.freemem(),
		hostname: os.hostname(),
		cpu: os.cpus(),
		freecpu,
		loadavg: os.loadavg(),
		account,
	})
}

let lastLines = [];

let latestLogUpdate = 0;

let scheduleReboot = false;

const checkScriptRuntime = async () => {

	const uptimeThreshold = 3600 * 6; /* 6 hours */

	if(os.uptime() >= uptimeThreshold && !scheduleReboot) {

		console.log('Online for over 6 hours... Setting reboot flag true');

		scheduleReboot = true;
	}
}

const startTime = new Date().getTime();

const checkLastLines = () => {

	if (!latestLogUpdate) {

		latestLogUpdate = new Date().getTime();
	}

	console.log('checking, last update', latestLogUpdate)

	const currentTime = new Date().getTime();

	const differenceInSeconds = (currentTime - latestLogUpdate) * .001;

	const startTimeInSeconds = (currentTime - startTime) * .001;

 	const timeoutTresholdInSeconds = (60 * 4);

	let shouldRestart = false;

	for(const line of lastLines) {

		if(line.includes('BANK') && scheduleReboot) {

			reboot();

			return;
		}
	}

	if (differenceInSeconds > timeoutTresholdInSeconds) {

		if (differenceInSeconds > timeoutTresholdInSeconds && startTimeInSeconds > timeoutTresholdInSeconds) {

			shouldRestart = true;

			latestLogUpdate = new Date().getTime();
		}
	}

	if(shouldRestart) {

		const { username } = getUserInfo();

		console.log('Detected script issues... Asking API for permission to restart...')

		socket.emit('script_issues_detected', {
			username
		});

		latestLogUpdate = new Date().getTime();
	}
}

const loop = async () => {

	try {

		checkLastLines();

		checkScriptRuntime();

		emitClientStats(socket);

	} catch (error) {

		console.warn({ error })
	}

	const timeout = setTimeout(() => {

		clearTimeout(timeout);

		process.nextTick(loop);

	}, 2500)
}

let lastLogSize = 0;

const watchLogs = () => {

	const { username } = getUserInfo();

	console.log('watching', process.env.LOG_PATH)

	chokidar.watch(process.env.LOG_PATH, { usePolling: true, persistent: true, interval: 2500 }).on('change', async (path, event) => {

		try {

			const lineCountResponse = execSync(`wc -l < "${path}"`);

			const lineCount = Number(lineCountResponse.toString().replace('\n', ''));

			if (!lastLogSize) {

				lastLogSize = lineCount;
			}

			const logLinesChanged = Math.max(lineCount - lastLogSize, 1);

			lastLogSize = lineCount;

			const lastLinesResponse = execSync(`tail -n ${logLinesChanged} "${path}"`);

			const lastLogLines = lastLinesResponse.toString();

			lastLines = [
				...lastLines,
				...lastLogLines.split('\n').filter(Boolean)
			].slice(-25);

			socket.emit('log_update', {
				socketId: socket.id,
				messages: lastLogLines.split('\n').filter(Boolean).map((message) => ({
					id: uuidv4(),
					message,
				})),
				username,
			})

			latestLogUpdate = new Date().getTime();

		} catch (error) {

			console.log({
				error
			})
		}
	})
}

watchLogs();

try {

	loop();

} catch (error) {

	console.log({
		loopError: erro
	})

}


console.log('Bot manager running...');
