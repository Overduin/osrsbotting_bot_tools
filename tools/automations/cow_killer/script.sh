# Cow killer script for AWS Linux 2 instances

# Get latest updates
sudo yum update -y
# Install java
sudo yum install java-1.8.0-openjdk -y
# Install virtual screen
sudo yum install xorg-x11-server-Xvfb -y
# Install git
sudo yum install git -y
# Download client
wget https://dreambot.org/DBLauncher.jar
# Make client executable
chmod u+x DBLauncher.jar
# Run initial client build and timeout after the files have been loaded
timeout 10 xvfb-run java -jar DBLauncher.jar
# Make folder for configs
mkdir -p ~/DreamBot/Data/Bun/AutomationTool/Profiles/
# Get repo
git clone https://gitlab.com/Overduin/osrsbotting_bot_tools.git
# Get latest changes just incase
cd ~/osrsbotting_bot_tools && git pull && cd ../
# Move the awslogs config file
# sudo cp ~/osrsbotting_bot_tools/tools/logs/awslogs.conf /etc/awslogs/awslogs.conf

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
# Refresh sources
source ~/.bashrc
# Install node 14
nvm install 14
# Install forever
npm i -g forever
# Kill all running forever scripts
forever stopall
# Create log dir
sudo mkdir -p ~/forever_logs
# Install tools packages
cd ~/osrsbotting_bot_tools/tools/

npm i

cd ~

sudo cp ~/osrsbotting_bot_tools/tools/.env.production ~/osrsbotting_bot_tools/tools/.env
# Start bot manager
cd ~/osrsbotting_bot_tools/tools/

forever start --uuid bot-manager ./src/index.js -l ~/forever_logs/forever.log -e ~/forever_logs/err.log -o ~/forever_logs/out.log

cd ~
# Move config files
sudo mkdir /DreamBot
sudo cp ~/osrsbotting_bot_tools/tools/automations/cow_killer/config/cow_killer_fightaholic_bronze.ini /DreamBot/cow_killer_fightaholic_bronze.ini
sudo cp ~/osrsbotting_bot_tools/tools/automations/cow_killer/config/cow_killer_fightaholic_steel.ini /DreamBot/cow_killer_fightaholic_steel.ini
sudo cp ~/osrsbotting_bot_tools/tools/automations/cow_killer/config/cow_killer_fightaholic_addy.ini /DreamBot/cow_killer_fightaholic_addy.ini
cp ~/osrsbotting_bot_tools/tools/automations/cow_killer/config/cow_slayer.cfg ~/DreamBot/Data/Bun/AutomationTool/cow_slayer.cfg
cp ~/osrsbotting_bot_tools/tools/automations/cow_killer/config/cow_slayer_mule.cfg ~/DreamBot/Data/Bun/AutomationTool/cow_slayer_mule.cfg
# Create folder for images
sudo mkdir -p /DreamBot/Scripts/Fightaholic/
# Create log path
sudo mkdir ~/DreamBot/Logs
# Give permissions
# Download images (required for fightaholic)
wget http://bot.files.s3.amazonaws.com/fightaholic_images.zip
# Move the zip to the correct folder
sudo mv fightaholic_images.zip /DreamBot/Scripts/Fightaholic
# Extract the images
cd /DreamBot/Scripts/Fightaholic

sudo unzip -o ./fightaholic_images.zip

sudo chown -hR ec2-user:ec2-user ~/DreamBot/
sudo chown -hR ec2-user:ec2-user /DreamBot/

cd ~

# Create cron for starting on boot
echo -e "@reboot sudo runuser -l ec2-user -c \x22cd /home/ec2-user/osrsbotting_bot_tools/tools/ && forever start src/index.js\x22" >> cron_bot_tools
crontab cron_bot_tools
rm cron_bot_tools


# Create file with credentials for quest script
sudo bash -c "echo \"$USERNAME:$PASSWORD\" > /credentials"
sudo bash -c "echo \"$DB_USERNAME:$DB_PASSWORD\" > /db_credentials"
# Output command
echo xvfb-run -a java -jar /DreamBot/BotData/client.jar -lowDetail -noClickWalk -script "Bun Automation Tool" -username "$DB_USERNAME" -password "$DB_PASSWORD" -covert -accountUsername "$USERNAME" -accountPassword "$PASSWORD" -render "none" -no-fresh -world f2p -params config=cow_slayer
# xvfb-run -a java -jar /DreamBot/BotData/client.jar -lowDetail -noClickWalk -script "Bun Automation Tool" -username "$DB_USERNAME" -password "$DB_PASSWORD" -covert -accountUsername "$USERNAME" -accountPassword "$PASSWORD" -render "none" -no-fresh -world f2p -params config=cow_slayer
