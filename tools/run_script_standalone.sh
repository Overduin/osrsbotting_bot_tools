# Cancel periodic updates
sudo su
printf "APT::Periodic::Update-Package-Lists \"0\";\nAPT::Periodic::Unattended-Upgrade \"0\";" > /etc/apt/apt.conf.d/20auto-upgrades
exit
# Stop snapd
sudo systemctl stop snapd.service
# Get latest updates
sudo apt-get update
# Install java
sudo apt-get install default-jre -y
# Install virtual screen
sudo apt-get install xvfb -y
# Install git
sudo apt-get install git -y
# Download client
rm DBLauncher.jar
wget https://dreambot.org/DBLauncher.jar
# Make client executable
chmod u+x DBLauncher.jar
# Run initial client build and timeout after the files have been loaded
timeout 10 xvfb-run java -jar DBLauncher.jar
# Make folder for configs
mkdir -p ./DreamBot/Data/Bun/AutomationToolEarlyAccess/
mkdir -p ./DreamBot/Data/Bun/Default/
# Get repo
git clone https://gitlab.com/Overduin/osrsbotting_bot_tools.git

# Move config files
cp /home/ubuntu/osrsbotting_bot_tools/docker/AutoGreenDragonhideTanner/configs/auto_dhide_tanner.cfg ./DreamBot/Data/Bun/AutomationToolEarlyAccess/auto_dhide_tanner.cfg
cp /home/ubuntu/osrsbotting_bot_tools/docker/AutoGreenDragonhideTanner/configs/green_dhide_tanner_config.cfg ./DreamBot/Data/Bun/Default/green_dhide_tanner.cfg
# Run bot
xvfb-run -a java -jar /home/ubuntu/DreamBot/BotData/client.jar -lowDetail -noClickWalk -script "Bun Automation Tool" -username "$DB_USERNAME" -password "$DB_PASSWORD" -covert -accountUsername "$USERNAME" -accountPassword "$PASSWORD" -render "none" -no-fresh -world members -params config=auto_dhide_tanner