import fs from 'fs';
import dotenv from 'dotenv'

dotenv.config();

export const getUserName = () => {

	const accountCredentialsPath = process.env.CREDENTIALS_PATH;

	const accountCredentials = fs.readFileSync(accountCredentialsPath, {
		encoding: 'utf-8',
		flag: 'r'
	});

	const [userName, password] = accountCredentials.split(':');

	return userName;
}