
import fs from 'fs';
import * as dotenv from 'dotenv';

dotenv.config();

export const getDBUserInfo = () => {

	const dreambotCredentialsPath = process.env.DB_CREDENTIALS_PATH;

	const dreambotCredentials = fs.readFileSync(dreambotCredentialsPath, {
		encoding: 'utf-8',
		flag: 'r'
	});

	const [dbUsername, dbPassword] = dreambotCredentials.replace('\n', '').split(':');

	return { dbUsername, dbPassword };
}

export const getUserInfo = () => {

	const accountCredentialsPath = process.env.CREDENTIALS_PATH;

	const accountCredentials = fs.readFileSync(accountCredentialsPath, {
		encoding: 'utf-8',
		flag: 'r'
	});

	const [username, password] = accountCredentials.replace('\n', '').split(':');

	return { username, password };
}

export const getAccountName = () => {

	const accountJsonDir = '/home/ec2-user/DreamBot/Scripts/Bun/AutomationTool';

	const files = fs.readdirSync(accountJsonDir);

	let jsonFileName = '';

	for (const file of files) {

		if(`${file}`.endsWith('.json') && !`${file}`.startsWith('summary')) {

			jsonFileName = file;
		}
	}

	return jsonFileName.replace('.json', '');
}