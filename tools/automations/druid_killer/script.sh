# Cow killer script for AWS Linux 2 instances

# Get latest updates
sudo yum update -y
# Install java
sudo yum install java-1.8.0-openjdk -y
# Install virtual screen
sudo yum install xorg-x11-server-Xvfb -y
# Install git
sudo yum install git -y
# Download client
wget https://dreambot.org/DBLauncher.jar
# Make client executable
chmod u+x DBLauncher.jar
# Run initial client build and timeout after the files have been loaded
timeout 10 xvfb-run java -jar DBLauncher.jar
# Get repo
git clone https://gitlab.com/Overduin/osrsbotting_bot_tools.git
# Get latest changes just incase
cd ~/osrsbotting_bot_tools && git pull && cd ../
# sudo cp ~/osrsbotting_bot_tools/tools/logs/awslogs.conf /etc/awslogs/awslogs.conf
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
# Refresh sources
source ~/.bashrc
# Install node 14
nvm install 14
# Install forever
npm i -g forever
# Kill all running forever scripts
forever stopall
# Create log dir
sudo mkdir -p ~/forever_logs
# Install tools packages
cd ~/osrsbotting_bot_tools/tools/
# Install packages
npm i
# Cd into home
cd ~
# Move env variables
sudo cp ~/osrsbotting_bot_tools/tools/.env.production ~/osrsbotting_bot_tools/tools/.env
# Start bot manager
cd ~/osrsbotting_bot_tools/tools/

# Move health check script
sudo cp ~/osrsbotting_bot_tools/tools/automations/druid_killer/health_check.sh /home/ec2-user/
forever start --uuid bot-manager ./src/index.js -l ~/forever_logs/forever.log -e ~/forever_logs/err.log -o ~/forever_logs/out.log

cd ~
# Move config files
sudo mkdir -p ~/DreamBot/SubUndead/
sudo mkdir -p /home/ec2-user/DreamBot/Scripts/Bun/AutomationTool
sudo cp -rf ~/osrsbotting_bot_tools/tools/automations/druid_killer/config/mule.cfg /home/ec2-user/DreamBot/Scripts/Bun/AutomationTool

sudo cp ~/osrsbotting_bot_tools/tools/automations/druid_killer/config/profile.json ~/DreamBot/SubUndead/
sudo cp ~/DreamBot/SubUndead/profile.json ~/DreamBot/SubUndead/profile.txt
sudo cp /home/ec2-user/osrsbotting_bot_tools/tools/automations/druid_killer/config/default.json /home/ec2-user/DreamBot/SubUndead/default.txt
sudo mkdir ~/DreamBot/Logs
# Give permissions
sudo chown -hR ec2-user:ec2-user ~/DreamBot/

cd ~
# Create cron for starting on boot
echo -e "@reboot sudo runuser -l ec2-user -c \x22cd /home/ec2-user/osrsbotting_bot_tools/tools/ && forever start src/index.js\x22" >> cron_bot_tools
echo -e "*/1 * * * * . /home/ec2-user/health_check.sh" >> cron_bot_tools
crontab cron_bot_tools
rm cron_bot_tools
# Create file with credentials for tool purposes
sudo bash -c "echo \"$USERNAME:$PASSWORD\" > /credentials"
sudo bash -c "echo \"$DB_USERNAME:$DB_PASSWORD\" > /db_credentials"
# Output command
echo xvfb-run -a java -jar /DreamBot/BotData/client.jar -lowDetail -noClickWalk -script "Sub Undead Druids AIO" -username "$DB_USERNAME" -password "$DB_PASSWORD" -covert -accountUsername "$USERNAME" -accountPassword "$PASSWORD" -render "none" -no-fresh -world f2p -params config=default
# xvfb-run -a java -jar /DreamBot/BotData/client.jar -lowDetail -noClickWalk -script "Bun Automation Tool" -username "$DB_USERNAME" -password "$DB_PASSWORD" -covert -accountUsername "$USERNAME" -accountPassword "$PASSWORD" -render "none" -no-fresh -world f2p -params config=cow_slayer
