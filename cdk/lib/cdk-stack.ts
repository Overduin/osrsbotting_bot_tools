import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class EC2BottingStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const vpc = this.createVPC();

    const sg = this.createSecurityGroup(vpc);
    // Allow SSH (TCP Port 22) access from anywhere

    const role = new cdk.aws_iam.Role(this, 'ec2Role', {
      assumedBy: new cdk.aws_iam.ServicePrincipal('ec2.amazonaws.com')
    })

    role.addManagedPolicy(cdk.aws_iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonSSMManagedInstanceCore'))

    const ami = new ec2.AmazonLinuxImage({
      generation: ec2.AmazonLinuxGeneration.AMAZON_LINUX,
      cpuType: ec2.AmazonLinuxCpuType.X86_64,

    });

    const ec2Instance = new ec2.Instance(this, 'Instance', {
      vpc,
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.MICRO),
      machineImage: ami,
      securityGroup: sg,
      // keyName: key.keyPairName,
      role: role
    });

    // Use

    // The code that defines your stack goes here

    // example resource
    // const queue = new sqs.Queue(this, 'CdkQueue', {
    //   visibilityTimeout: cdk.Duration.seconds(300)
    // });
  }

  createSecurityGroup(vpc: ec2.Vpc) {

    const securityGroup = new ec2.SecurityGroup(this, 'SecurityGroup', {
      vpc,
      description: 'Allow SSH (TCP port 22) in',
      allowAllOutbound: true,
    });

    securityGroup.addIngressRule(ec2.Peer.ipv4('94.210.139.222/16'), ec2.Port.tcp(22), 'Allow SSH Access')

    return securityGroup;
  }

  createVPC() {

    return new ec2.Vpc(this, 'BotStackVPC', {
      natGateways: 0,
      subnetConfiguration: [{
        cidrMask: 16,
        name: "asterisk",
        subnetType: ec2.SubnetType.PUBLIC
      }]
    });
  }
}
