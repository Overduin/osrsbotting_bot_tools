# Import required packages
import cv2
import pytesseract
import numpy as np
from os.path import isfile
import os
import json

# Mention the installed location of Tesseract-OCR in your system
pytesseract.pytesseract.tesseract_cmd = '/usr/bin/tesseract'

# Read image from which text needs to be extracted
imglist = sorted(os.listdir(os.getcwd() + '/screenshots'))

imglist.reverse()

#print(imglist)

if len(imglist) >= 1:

	data = {
			"teleblockData": [],
			"scriptData": [],
			"lootData": []
	}

	for imglocation in imglist:

		# print("scanning" + imglocation)

		img = cv2.imread(os.getcwd() + '/screenshots/' + imglocation)

		# TB text (Purple)
		teleblock_text_color = np.array([239,86,195])
		# Valuable loot notification text (Red)
		valuable_loot_text_color = np.array([32,16,239])
		# Script paint overlay text color (Almost perfect white)
		script_paint_text_color = np.array([255,251,255])

		# Extract all script paint text
		script_paint_text_mask = cv2.inRange(img, script_paint_text_color, script_paint_text_color)
		# Extract all TB text
		teleblock_text_mask = cv2.inRange(img, teleblock_text_color, teleblock_text_color)
		# Extract all valuable loot text
		valuable_loot_text_mask = cv2.inRange(img, valuable_loot_text_color, valuable_loot_text_color)

		# cv2.imshow("gg", script_paint_text_mask)
		# cv2.waitKey()

		pytesseract_config = "-c tessedit_char_whitelist=\"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ():.,[]\" --psm 11"

		script_paint_text = pytesseract.image_to_string(script_paint_text_mask, lang="eng", config=pytesseract_config)

		teleblock_text = pytesseract.image_to_string(teleblock_text_mask, lang="eng", config=pytesseract_config)

		valuable_loot_text = pytesseract.image_to_string(valuable_loot_text_mask,lang="eng", config=pytesseract_config)

		for text in script_paint_text.splitlines():

			if len(text) >=1:

					data["scriptData"].append(text)

		for text in teleblock_text.splitlines():

			if len(text) >=1:

					data["teleblockData"].append(text)

		for text in valuable_loot_text.splitlines():

			if len(text) >=1:

					data["lootData"].append(text)

		os.remove(os.getcwd() + '/screenshots/' + imglocation)

	print(json.dumps(data))
