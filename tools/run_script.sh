# Get latest updates
sudo apt-get update
# Install java
sudo apt-get install default-jre -y
# Install virtual screen
sudo apt-get install xvfb -y
# Install git
sudo apt-get install git -y
# Download client
rm DBLauncher.jar
wget https://dreambot.org/DBLauncher.jar
# Make client executable
chmod u+x DBLauncher.jar
# Run initial client build and timeout after the files have been loaded
timeout 10 xvfb-run java -jar DBLauncher.jar
# Make folder for configs
mkdir -p ./DreamBot/Data/Bun/AutomationToolEarlyAccess/
# Paste user/pass in a file
# Get repo
git clone https://gitlab.com/Overduin/osrsbotting_bot_tools.git

cd ~/osrsbotting_bot_tools && git pull && cd ../
# Move the awslogs config file
sudo cp ~/osrsbotting_bot_tools/tools/logs/awslogs.conf /etc/awslogs/awslogs.conf
# Start aws logging
sudo service awslogsd start
# Enable it
sudo systemctl enable awslogsd
# Move config files
cp ~/osrsbotting_bot_tools/tools/automations/cow_killer/config/cow_killer_fightaholic_bronze.ini ~/DreamBot/cow_killer_fightaholic_bronze.ini
cp ~/osrsbotting_bot_tools/tools/automations/cow_killer/config/cow_killer_fightaholic_steel.ini ~/DreamBot/cow_killer_fightaholic_steel.ini
cp ~/osrsbotting_bot_tools/tools/automations/cow_killer/config/cow_killer_fightaholic_addy.ini ~/DreamBot/cow_killer_fightaholic_addy.ini
cp ~/osrsbotting_bot_tools/tools/automations/cow_killer/config/cow_slayer.cfg ~/DreamBot/Data/Bun/AutomationToolEarlyAccess/cow_slayer.cfg
# Output command
echo xvfb-run -a java -jar ~/DreamBot/BotData/client.jar -lowDetail -noClickWalk -script "Bun Automation Tool" -username "$DB_USERNAME" -password "$DB_PASSWORD" -covert -accountUsername "$USERNAME" -accountPassword "$PASSWORD" -render "none" -no-fresh -world f2p -params config=cow_slayer
# Run bot
xvfb-run -a java -jar ~/DreamBot/BotData/client.jar -lowDetail -noClickWalk -script "Bun Automation Tool" -username "$DB_USERNAME" -password "$DB_PASSWORD" -covert -accountUsername "$USERNAME" -accountPassword "$PASSWORD" -render "none" -no-fresh -world f2p -params config=cow_slayer
