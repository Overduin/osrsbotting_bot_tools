import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import { createClient } from 'redis';
import * as dotenv from 'dotenv';
import { Server } from 'socket.io';
import { handleDreambotScripts } from './dreambot_scripts.js';
import { handleTribotScripts } from './tribot_scripts.js';

dotenv.config();

const redisClient = createClient({
	url: process.env.REDIS_URL,
});

await redisClient.connect();

redisClient.on('error', (err) => console.log('Redis Client Error', err));

dotenv.config();

const port = 1337;

const app = express();

app.use(bodyParser.json({
	extended: true,
	limit: '100mb',
	parameterLimit: 10e9,
}));

app.use(bodyParser.urlencoded({
	extended: true,
	limit: '100mb',
	parameterLimit: 10e9,
}));

app.use((req, res, next) => {

	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Credentials", "true");
	res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
	res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization");
	next();
});

const httpServer = http.createServer(app);

const io = new Server(httpServer, {
	cors: {
		origin: "*"
	}
});

httpServer.listen(port, () => {

	console.log(`API Running on port ${port}`);
})

handleTribotScripts(app, redisClient, io);

handleDreambotScripts(app, redisClient, io);




